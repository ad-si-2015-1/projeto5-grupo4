var fs = require('fs')
    , http = require('http')
    , socketio = require('socket.io');

global.rodada = 0;
global.jogadores = [];
global.palavras = [];
global.letrasAcertadas = [];
global.letrasUtilizadas = [];
global.palavra = { "nome":"", "categoria":0 };
global.jogadorVez = '';

function mascararPalavra(palavra) {
	var palavraMascarada = '';
	var letra = true;
	for(i = 0; i< palavra.length; i++){
		for (j = 0; j < global.letrasAcertadas.length; j++) {
			if (palavra.charAt(i) == global.letrasAcertadas[j]) {
				palavraMascarada = palavraMascarada + palavra.charAt(i);
				letra = false;
			}
		}
		if (letra) {
			palavraMascarada = palavraMascarada + "_ ";
		}
		letra = true;
	}
	return 'A palavra eh ' + palavraMascarada + ' e a categoria e ' + global.palavra.categoria + '.';
}

function pontuar(jogador) {
	if (global.jogadores[0].nome == global.jogadorVez) {
		global.jogadores[0].pontos++;
	} else if (global.jogadores[1].nome == global.jogadorVez) {
		global.jogadores[1].pontos++;
	} else if (global.jogadores[2].nome == global.jogadorVez) {
		global.jogadores[2].pontos++;
	}
}

function trocarVez() {
	if (global.jogadores[0].nome == global.jogadorVez) {
		jogadorVez = global.jogadores[1].nome;
	} else if (global.jogadores[1].nome == global.jogadorVez) {
		jogadorVez = global.jogadores[2].nome;
	} else if (global.jogadores[2].nome == global.jogadorVez) {
		jogadorVez = global.jogadores[0].nome;
	}
}

function gerarPalavra() {
	var rand = Math.floor(Math.random()*palavras.length); // gerando o número aleatório
	console.log ('numero sorteado ' + rand); //pritando número aleatório
	global.palavra.nome = palavras[rand].nome; //passando o nome da palavra sorteada do array para a variavel palavra
	global.palavra.categoria = palavras[rand].categoria; //passando a categoria da palavra sorteada para a variavel palavra
	rodada++; //aumenta a rodada
	console.log ('palavra sorteada ' + palavra.nome);
}

function inicializarPalavras () {
	global.palavras.push({'nome': 'carro', 'categoria': 'veiculo'});
	global.palavras.push({'nome': 'cachorro', 'categoria': 'animal'});
	global.palavras.push({'nome': 'cadeira', 'categoria': 'objeto'});
	global.palavras.push({'nome': 'mesa', 'categoria': 'objeto'});
	global.palavras.push({'nome': 'gato', 'categoria': 'animal'});
	global.palavras.push({'nome': 'hipopotomo', 'categoria': 'animal'});
	global.palavras.push({'nome': 'elefante', 'categoria': 'animal'});
	global.palavras.push({'nome': 'melancia', 'categoria': 'fruta'});
	global.palavras.push({'nome': 'banana', 'categoria': 'fruta'});
	global.palavras.push({'nome': 'laranja', 'categoria': 'fruta'});
	global.palavras.push({'nome': 'cama', 'categoria': 'objeto'});
	global.palavras.push({'nome': 'armario', 'categoria': 'objeto'});
	global.palavras.push({'nome': 'celta', 'categoria': 'carro'});
	global.palavras.push({'nome': 'palio', 'categoria': 'carro'});
	global.palavras.push({'nome': 'corvette', 'categoria': 'carro'});
	global.palavras.push({'nome': 'fiesta', 'categoria': 'carro'});
	global.palavras.push({'nome': 'mamao', 'categoria': 'fruta'});
	global.palavras.push({'nome': 'vaca', 'categoria': 'animal'});
	global.palavras.push({'nome': 'morcego', 'categoria': 'animal'});
	global.palavras.push({'nome': 'porco', 'categoria': 'animal'});
}

function iniciarJogo() {
	inicializarPalavras();
	gerarPalavra();
	global.jogadorVez = jogadores[0].nome; //pegando o nome do primeiro jogador a entrar para passar a vez pra ele
}
 
var server = http.createServer(function(req, res) {
    res.writeHead(200, { 'Content-type': 'text/html'});
    res.end(fs.readFileSync(__dirname + '/index.html'));
}).listen(8080, function() {
    console.log('Listening at: http://localhost:8080');
});
 
socketio.listen(server).on('connection', function (socket) {
    socket.on('message', function (msg) {
        console.log('Message Received: ', msg);
        socket.broadcast.emit('message', msg);
    });
    socket.on('logar', function (msg) {
        console.log('Usuario logado: '+ msg);
		if (jogadores.length < 3) {
			
			global.jogadores.push({'id': socket.id, 
							   'nome': msg, 
							   'pontos': 0});
			socket.broadcast.emit('message', 'Jogador '+msg+' entrou no jogo'); //imprime pra todos menos pra quem solicitou
			socket.emit('message', 'Jogador '+msg+' entrou no jogo'); //imprime pra quem solicitou
			if (jogadores.length == 3) {
				iniciarJogo();
				socket.emit('tela', mascararPalavra(palavra.nome) + ' e a vez do jogador ' + global.jogadorVez);
				socket.broadcast.emit('tela', mascararPalavra(palavra.nome) + ' e a vez do jogador ' + global.jogadorVez);
			}
		} else {
			socket.emit('message', 'Servidor esta cheio. Teve novamente mais tarde'); //imprime pra quem solicitou que está cheio
		}
	});
	socket.on('tela', function (tela) {
		socket.broadcast.emit('tela', tela);
	});
    socket.on('responder', function ( objResposta ) {
		var letraRepetida = false;
        if (objResposta.nome == global.jogadorVez) {
			if (objResposta.resposta.length == 1) { //verificar se digitou só uma letra
				for (i = 0; i < global.letrasUtilizadas.length; i++) { //loop para conferir se a letra já foi utilizada
					if (global.letrasUtilizadas[i] == objResposta.resposta) { //verifica se a letra tentada já foi utilizada
						letraRepetida = true; //caso positovo ativa o flag
						socket.emit('tela', 'Essa letra ja foi utilizada. Tente novamente');
					}
				}
				
				if (!letraRepetida) {
					global.letrasUtilizadas.push(objResposta.resposta); //add a letra digita para as letras utilizadas
					if (global.palavra.nome.indexOf(objResposta.resposta) > -1) { //verifica se a letra digitada contem na palavra sorteada
						global.letrasAcertadas.push(objResposta.resposta);
						socket.emit('tela', 'O jogador ' + global.jogadorVez + ' acertou a letra ' + objResposta.resposta + '. ' + mascararPalavra(palavra.nome) + ' A vez ainda e dele.');
						socket.broadcast.emit('tela', 'O jogador ' + global.jogadorVez + ' acertou a letra ' + objResposta.resposta + '. ' + mascararPalavra(palavra.nome) + ' A vez ainda e dele.');
					} else {
						var jogadorErrou = global.jogadorVez;
						trocarVez();
						socket.emit('tela', 'O jogador ' + jogadorErrou + ' errou a letra. ' + mascararPalavra(palavra.nome) + ' A vez agora e do jogador ' + global.jogadorVez);
						socket.broadcast.emit('tela', 'O jogador ' + jogadorErrou + ' errou a letra. ' + mascararPalavra(palavra.nome) + ' A vez agora e do jogador ' + global.jogadorVez);
					}
				}
			} else { //caso digitou a palavra toda
				if (objResposta.resposta == global.palavra.nome) {
					socket.emit('tela', 'O jogador ' + global.jogadorVez + ' acertou a palavra que era ' + global.palavra.nome);
					socket.broadcast.emit('tela', 'O jogador ' + global.jogadorVez + ' acertou a palavra que era ' + global.palavra.nome);
					pontuar(); //pontuacao
					if (rodada < 5) { //começar outra rodada
						gerarPalavra(); //gerando nova palavra
						global.letrasAcertadas = []; //limpando array das letras acertadas
						global.letrasUtilizadas = []; //limpando array das letras utilizadas
						trocarVez(); //passando a vez pra um outro jogador
						socket.emit('tela', mascararPalavra(palavra.nome) + ' e a vez do jogador ' + global.jogadorVez);
						socket.broadcast.emit('tela', mascararPalavra(palavra.nome) + ' e a vez do jogador ' + global.jogadorVez);
					} else { //fim do jogo
						socket.emit('tela', 'Jogador ' + global.jogadores[0].nome + ' fez ' + global.jogadores[0].pontos + ' pontos. Jogador ' + global.jogadores[1].nome + ' fez ' + global.jogadores[1].pontos + 'ponto(s). Jogador ' + global.jogadores[2].nome + ' fez ' + global.jogadores[2].pontos + ' ponto(s)');
						socket.broadcast.emit('tela', 'Jogador ' + global.jogadores[0].nome + ' fez ' + global.jogadores[0].pontos + ' pontos. Jogador ' + global.jogadores[1].nome + ' fez ' + global.jogadores[1].pontos + 'ponto(s). Jogador ' + global.jogadores[2].nome + ' fez ' + global.jogadores[2].pontos + ' ponto(s)');
					}
				} else {
					var jogadorErrou = global.jogadorVez;
					trocarVez();
					socket.emit('tela', 'O jogador ' + jogadorErrou + ' errou a palavra. ' + mascararPalavra(palavra.nome) + ' e a vez do jogador ' + global.jogadorVez);
					socket.broadcast.emit('tela', 'O jogador ' + jogadorErrou + ' errou a palavra. ' + mascararPalavra(palavra.nome) + ' e a vez do jogador ' + global.jogadorVez);
				}
			}
		} else { //caso não seja a vez do jogador
			socket.emit('tela', 'Calma manolo! Nao e a sua vez!');
		}
	});

});
